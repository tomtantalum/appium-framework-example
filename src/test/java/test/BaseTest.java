package test;

import driver.DriverFactory;
import io.appium.java_client.AppiumDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public abstract class BaseTest {
    protected AppiumDriver driver;

    @BeforeMethod
    public void setUp() throws Exception {
        String platform = System.getProperty("platform", "android");
        driver = new DriverFactory().createInstance(platform);
    }

    @AfterMethod
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
