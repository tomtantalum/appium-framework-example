package driver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class DriverFactory {
    public AppiumDriver createInstance(String platform) throws MalformedURLException {
        switch (platform.toLowerCase()) {
            case "android":
                return androidDriver();
            case "ios":
                return iosDriver();
            default:
                return androidDriver();
        }
    }

    private AndroidDriver androidDriver() throws MalformedURLException {
        File app = new File(System.getProperty("user.dir") + "/app/android/app-debug.apk");
        URL url = new URL("http://localhost:4723/wd/hub");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Android Device");
        capabilities.setCapability("app", app);

        return new AndroidDriver(url, capabilities);
    }

    private IOSDriver iosDriver() throws MalformedURLException {
        File app = new File(System.getProperty("user.dir") + "/app/ios/app-debug.app");
        URL url = new URL("http://localhost:4723/wd/hub");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("deviceName", "iOS Device");
        capabilities.setCapability("app", app);

        return new IOSDriver(url, capabilities);
    }
}
